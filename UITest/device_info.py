from appium import webdriver

#///////////////////////// iPhone SE iOS 11///////////////////////////
iPhone_se = {
    "platformName": "iOS",
    "deviceName": "iPhone se",
    "automationName": "XCUITest",
    "bundleId": "com.appier.ABC",
    "xcodeOrgId": "V72QYCH5UU",
    "xcodeSigningId": "iPhone Developer: Appier QA (926GQY2S5J)",
    "udid": "a8a85518f997d4665dc361ca2c3754a0593ae3c7"
}
caps_iphone_se = webdriver.Remote('http://localhost:4723/wd/hub', iPhone_se)


#///////////////////////// Shiv's iPhone 7 ///////////////////////////
iPhone_7 = {
    "platformName": "iOS",
    "deviceName": "iPhone 7 real",
    "automationName": "XCUITest",
    "bundleId": "com.appier.ABC",
    "xcodeOrgId": "V72QYCH5UU",
    "xcodeSigningId": "iPhone Developer: Appier QA (926GQY2S5J)",
    "udid": "775d6dd160e59e4f1120a82d386007ee3729cdb1"
}
caps_iphone_7 = webdriver.Remote('http://localhost:4723/wd/hub', iPhone_7)


