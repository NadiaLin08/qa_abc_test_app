// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNotificationsManager.h"

#import "QGFBNConstants.h"
#import "QGFBNCardError.h"
#import "QGFBNAssetContentCache.h"
#import "QGFBNCardViewUtilities.h"
#import "QGFBNCardViewController_Internal.h"
#import "QGFBNCardPayload.h"
#import "QGFBNCardAppEventsLogger.h"
#import "QGFBNAssetsController.h"
#import "QGFBNColorAsset.h"
#import "QGFBNColorAssetController.h"
#import "QGFBNImageAsset.h"
#import "QGFBNImageAssetController.h"
#import "QGFBNGIFAsset.h"
#import "QGFBNGIFAssetController.h"

@interface QGFBNotificationsManager ()<QGFBNCardViewControllerDelegate>

@property (nonnull, nonatomic, strong, readonly) QGFBNAssetsController *assetsController;

@property (nullable, nonatomic, weak) QGFBNCardViewController *currentCardViewController;

@end

@implementation QGFBNotificationsManager

///--------------------------------------
#pragma mark - Creating a Card Manager
///--------------------------------------

- (instancetype)init {
    self = [super init];
    if (!self) return self;

    _assetsController = [self _defaultConfigurationAssetsController];

    return self;
}

+ (instancetype)sharedManager {
    static QGFBNotificationsManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

///--------------------------------------
#pragma mark - Assets Controller
///--------------------------------------

- (QGFBNAssetsController *)_defaultConfigurationAssetsController {
    QGFBNAssetsController *controller = [[QGFBNAssetsController alloc] init];
    [controller registerAssetController:[[QGFBNColorAssetController alloc] init] forAssetType:QGFBNColorAssetType];
    [controller registerAssetController:[[QGFBNImageAssetController alloc] init] forAssetType:QGFBNImageAssetType];
    [controller registerAssetController:[[QGFBNGIFAssetController alloc] init] forAssetType:QGFBNGIFAssetType];
    return controller;
}

///--------------------------------------
#pragma mark - Present from Remote Notification
///--------------------------------------

- (void)preparePushCardContentForRemoteNotificationPayload:(NSDictionary *)payload
                                                completion:(nullable QGFBNCardContentPreparationCompletion)completion {
    if (![self canPresentPushCardFromRemoteNotificationPayload:payload]) {
        if (completion) {
            completion(nil, [QGFBNCardError invalidRemoteNotificationPayloadError]);
        }
        return;
    }
    [self.assetsController cacheAssetContentForCardPayload:payload completion:^{
        if (completion) {
            completion(payload, nil);
        }
    }];
}

- (void)presentPushCardForRemoteNotificationPayload:(NSDictionary *)payload
                                 fromViewController:(nullable UIViewController *)viewController
                                         completion:(nullable QGFBNCardPresentationCompletion)completion {
    if (![self canPresentPushCardFromRemoteNotificationPayload:payload]) {
        if (completion) {
            completion(nil, [QGFBNCardError invalidRemoteNotificationPayloadError]);
        }
        return;
    }

    QGFBNCardPayload *cardPayload = QGFBNCardPayloadFromRemoteNotificationPayload(payload);
    NSString *campaignIdentifier = [QGFBNCardAppEventsLogger campaignIdentifierFromRemoteNotificationPayload:payload];

    QGFBNCardViewController *oldCardViewController = self.currentCardViewController;
    // Has the same payload and is visible on screen, meaning that we don't need to re-present it.
    if ([oldCardViewController.payload isEqual:cardPayload] &&
        oldCardViewController.presentingViewController != nil &&
        !oldCardViewController.isBeingDismissed) {
        if (completion) {
            completion(oldCardViewController, nil);
        }
        return;
    }

    // Dismiss the old one.
    [oldCardViewController dismissViewControllerAnimated:NO completion:nil];

    // Create and present the new one.
    QGFBNCardViewController *cardViewController = [[QGFBNCardViewController alloc] initWithPushCardPayload:cardPayload
                                                                                    campaignIdentifier:campaignIdentifier
                                                                                      assetsController:self.assetsController];
    viewController = viewController ?: QGFBNApplicationTopMostViewController();
    BOOL animated = (oldCardViewController == nil);
    [viewController presentViewController:cardViewController animated:animated completion:^{
        if (completion) {
            completion(cardViewController, nil);
        }
    }];
    // Save the new one into a weak property.
    self.currentCardViewController = cardViewController;
    self.currentCardViewController.delegate = self;
}

- (BOOL)canPresentPushCardFromRemoteNotificationPayload:(nullable NSDictionary *)payload {
    QGFBNCardPayload *cardPayload = QGFBNCardPayloadFromRemoteNotificationPayload(payload);
    return (cardPayload != nil && QGFBNCardPayloadIsCompatibleWithCurrentVersion(cardPayload, QGFBNotificationsCardFormatVersionString));
}

///--------------------------------------
#pragma mark - Present via Local Notification
///--------------------------------------

- (void)createLocalNotificationFromRemoteNotificationPayload:(NSDictionary *)payload
                                                  completion:(QGFBNLocalNotificationCreationCompletion)completion {
    if (![self canPresentPushCardFromRemoteNotificationPayload:payload]) {
        NSError *error = [QGFBNCardError invalidRemoteNotificationPayloadError];
        completion(nil, error);
        return;
    }

    QGFBNCardPayload *cardPayload = QGFBNCardPayloadFromRemoteNotificationPayload(payload);
    [self.assetsController cacheAssetContentForCardPayload:cardPayload completion:^{
        UILocalNotification *notification = [self _localNotificationFromPayload:cardPayload];
        notification.userInfo = payload;
        completion(notification, nil);
    }];
}

- (void)presentPushCardForLocalNotification:(UILocalNotification *)notification
                         fromViewController:(nullable UIViewController *)viewController
                                 completion:(nullable QGFBNCardPresentationCompletion)completion {
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
    [self presentPushCardForRemoteNotificationPayload:notification.userInfo
                                   fromViewController:viewController
                                           completion:completion];
}

- (BOOL)canPresentPushCardFromLocalNotification:(UILocalNotification *)notification {
    return [self canPresentPushCardFromRemoteNotificationPayload:notification.userInfo];
}

- (nullable UILocalNotification *)_localNotificationFromPayload:(NSDictionary<NSString *, id> *)payload {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertTitle = payload[@"alert"][@"title"];
    notification.alertBody = payload[@"alert"][@"body"];
    return notification;
}

//added by Shiv
///--------------------------------------
#pragma mark - Remove the displaying FB Notification Card
///--------------------------------------
- (void)removeFBNCardViewController {
    QGFBNCardViewController *oldCardViewController = self.currentCardViewController;
    [oldCardViewController dismissViewControllerAnimated:NO completion:nil];
}

///--------------------------------------
#pragma mark - QGFBNCardViewControllerDelegate
///--------------------------------------
//added by shiv
- (void)pushCardViewControllerDidDismissWithOpenURL:(nullable NSURL *)url {
    id <FBCardDismissDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(fbPushCardDidDismissWithOpenURL:)]) {
        [delegate fbPushCardDidDismissWithOpenURL:url];
    }
    
}

@end
