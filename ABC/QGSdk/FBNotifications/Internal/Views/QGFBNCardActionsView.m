// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardActionsView.h"

#import "QGFBNAssetsController.h"
#import "QGFBNCardActionsConfiguration.h"
#import "QGFBNCardActionConfiguration.h"
#import "QGFBNCardActionButton.h"
#import "QGFBNCardViewUtilities.h"

@interface QGFBNCardActionsView ()

@property (nonatomic, strong, readonly) QGFBNCardActionsConfiguration *configuration;

@property (nonatomic, strong, readonly) UIView *backgroundView;
@property (nonatomic, copy, readonly) NSArray<QGFBNCardActionButton *> *actionButtons;

@end

@implementation QGFBNCardActionsView

///--------------------------------------
#pragma mark - Init
///--------------------------------------

- (instancetype)initWithConfiguration:(QGFBNCardActionsConfiguration *)configuration
                     assetsController:(QGFBNAssetsController *)assetsController
                             delegate:(id<QGFBNCardActionsViewDelegate>)delegate {
    self = [super init];
    if (!self) return self;

    _configuration = configuration;
    _delegate = delegate;

    id<QGFBNAsset> background = configuration.background;
    if (background) {
        _backgroundView = [assetsController viewForAsset:background];
        [self addSubview:_backgroundView];
    }

    NSMutableArray<QGFBNCardActionButton *> *actionButtons = [NSMutableArray arrayWithCapacity:configuration.actions.count];
    QGFBNCardButtonAction buttonAction = QGFBNCardButtonActionPrimary;
    for (QGFBNCardActionConfiguration *actionConfiguration in configuration.actions) {
        QGFBNCardActionButton *button = nil;
        if (actionConfiguration.actionURL != nil) {
            button = [QGFBNCardActionButton buttonFromConfiguration:actionConfiguration
                                                 withCornerRadius:configuration.cornerRadius
                                                           action:buttonAction];
            if (buttonAction == QGFBNCardButtonActionPrimary) {
                buttonAction = QGFBNCardButtonActionSecondary;
            }
        } else {
            button = [QGFBNCardActionButton buttonFromConfiguration:actionConfiguration
                                                 withCornerRadius:configuration.cornerRadius
                                                           action:QGFBNCardButtonActionDismiss];
        }
        [button addTarget:self action:@selector(_buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [actionButtons addObject:button];
        [self addSubview:button];
    }
    _actionButtons = [actionButtons copy];

    return self;
}

- (void)dealloc {
    for (QGFBNCardActionButton *button in _actionButtons) {
        [button removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    }
}

///--------------------------------------
#pragma mark - Layout
///--------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];

    const CGRect bounds = self.bounds;

    self.backgroundView.frame = bounds;

    CGSize buttonSize = { .width = 0.0f, .height = self.configuration.height };
    switch (self.configuration.layoutStyle) {
        case QGFBNCardActionsLayoutStyleHorizontal: {
            if (self.configuration.style == QGFBNCardActionsStyleAttached) {
                buttonSize.width = CGRectGetWidth(bounds) - self.configuration.contentInset * (self.actionButtons.count + 1); // Left + Right + Inter-button
            } else {
                buttonSize.width = CGRectGetWidth(bounds) - self.configuration.contentInset * (self.actionButtons.count - 1); // Inter-button only
            }
            buttonSize.width /= self.actionButtons.count;
        } break;
        case QGFBNCardActionsLayoutStyleVertical: {
            if (self.configuration.style == QGFBNCardActionsStyleAttached) {
                buttonSize.width = CGRectGetWidth(bounds) - (self.configuration.contentInset * 2); // Left + Right
            } else {
                buttonSize.width = CGRectGetWidth(bounds);
            }
        } break;
        default:break;
    }

    CGPoint buttonOrigin = { .x = 0.0f, .y = self.configuration.topInset };
    CGRect buttonFrame = QGFBNRectAdjustToScreenScale(QGFBNRectMakeWithOriginSize(buttonOrigin, buttonSize), NSRoundUp);

    if (self.configuration.style == QGFBNCardActionsStyleAttached) {
        buttonFrame.origin.x = self.configuration.contentInset;
    }

    for (QGFBNCardActionButton *button in self.actionButtons) {
        button.frame = QGFBNRectAdjustToScreenScale(buttonFrame, NSRoundUp);

        switch (self.configuration.layoutStyle) {
            case QGFBNCardActionsLayoutStyleHorizontal: {
                buttonFrame.origin.x = CGRectGetMaxX(buttonFrame) + self.configuration.contentInset; // Previous button + Inset
            } break;
            case QGFBNCardActionsLayoutStyleVertical: {
                buttonFrame.origin.y = CGRectGetMaxY(buttonFrame) + self.configuration.contentInset; // Previous button + Inset
            } break;
            default:break;
        }
    }
}

- (CGSize)sizeThatFits:(CGSize)fitSize {
    CGSize size = CGSizeMake(fitSize.width, self.configuration.topInset);
    switch (self.configuration.layoutStyle) {
        case QGFBNCardActionsLayoutStyleVertical: {
            size.height += self.configuration.height * self.actionButtons.count; // All the buttons
            size.height += self.configuration.contentInset * self.actionButtons.count; // Inter-button + Bottom
        } break;
        case QGFBNCardActionsLayoutStyleHorizontal: {
            size.height += self.configuration.height; // Single button
            size.height += self.configuration.contentInset; // Bottom inset
        } break;
        default: break;
    }
    return QGFBNSizeAdjustToScreenScale(size, NSRoundUp);
}

///--------------------------------------
#pragma mark - Button Action
///--------------------------------------

- (void)_buttonAction:(QGFBNCardActionButton *)button {
    [self.delegate actionsView:self didPerformButtonAction:button.action withOpenURL:button.configuration.actionURL];
}

@end
