// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardHeroView.h"

#import "QGFBNAssetsController.h"
#import "QGFBNCardHeroConfiguration.h"
#import "QGFBNCardViewUtilities.h"
#import "QGFBNCardLabel.h"

@interface QGFBNCardHeroView ()

@property (nonatomic, strong, readonly) QGFBNCardHeroConfiguration *configuration;
@property (nonatomic, assign, readonly) CGFloat contentInset;

@property (nullable, nonatomic, strong, readonly) UIView *backgroundView;
@property (nonatomic, strong, readonly) UILabel *textLabel;

@end

@implementation QGFBNCardHeroView

///--------------------------------------
#pragma mark - Init
///--------------------------------------

- (instancetype)initWithConfiguration:(QGFBNCardHeroConfiguration *)configuration
                     assetsController:(QGFBNAssetsController *)assetsController
                         contentInset:(CGFloat)contentInset {
    self = [super init];
    if (!self) return self;

    self.clipsToBounds = YES;

    _configuration = configuration;
    _contentInset = contentInset;

    _backgroundView = [assetsController viewForAsset:configuration.background];
    [self addSubview:_backgroundView];

    if (configuration.content) {
        _textLabel = [QGFBNCardLabel labelFromTextContent:configuration.content];
        [self addSubview:_textLabel];
    }

    return self;
}

///--------------------------------------
#pragma mark - Layout
///--------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];

    _backgroundView.frame = self.bounds;

    CGRect contentBounds = CGRectInset(self.bounds, self.contentInset, self.contentInset);

    const CGSize textLabelSize = CGSizeMake(CGRectGetWidth(contentBounds), [self.textLabel sizeThatFits:contentBounds.size].height);
    CGRect textLabelFrame = CGRectZero;
    switch (self.configuration.contentVerticalAlignment) {
        case QGFBNCardContentVerticalAlignmentTop: {
            textLabelFrame = QGFBNRectMakeWithOriginSize(CGPointMake(self.contentInset, self.contentInset), textLabelSize);
        } break;
        case QGFBNCardContentVerticalAlignmentCenter: {
            textLabelFrame = QGFBNRectMakeWithSizeCenteredInRect(textLabelSize, contentBounds);
        } break;
        case QGFBNCardContentVerticalAlignmentBottom: {
            CGPoint origin = CGPointMake(self.contentInset, CGRectGetMaxY(contentBounds) - textLabelSize.height);
            textLabelFrame = QGFBNRectMakeWithOriginSize(origin, textLabelSize);
        } break;
        default:break;
    }

    self.textLabel.frame = textLabelFrame;
}

- (CGSize)sizeThatFits:(CGSize)fitSize {
    CGSize labelFitSize = fitSize;
    labelFitSize.width -= self.contentInset * 2;
    labelFitSize.height -= self.contentInset * 2;

    CGSize textSize = [self.textLabel sizeThatFits:labelFitSize];
    if (self.textLabel.text.length > 0) {
        textSize.width += self.contentInset * 2;
        textSize.height += self.contentInset * 2;
    }

    CGSize imageSize = [self.backgroundView sizeThatFits:fitSize];
    if (imageSize.width != 0 && imageSize.height != 0) {
        CGFloat imageScale = QGFBNAspectFillScaleThatFits(imageSize, fitSize);
        imageSize.width *= imageScale;
        imageSize.height *= imageScale;
    }

    CGSize size = QGFBNSizeMax(textSize, imageSize);
    return QGFBNSizeAdjustToScreenScale(size, NSRoundUp);
}

@end
