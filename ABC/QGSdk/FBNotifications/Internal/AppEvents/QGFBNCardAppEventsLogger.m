// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardAppEventsLogger.h"
#import "QGSdk.h"

NS_ASSUME_NONNULL_BEGIN

@interface QGFBSDKAppEvents : NSObject

+ (void)logEvent:(NSString *)eventName parameters:(nullable NSDictionary<NSString *, NSString *> *)parameters;

@end

@implementation QGFBNCardAppEventsLogger

///--------------------------------------
#pragma mark - Public
///--------------------------------------

+ (nullable NSString *)campaignIdentifierFromRemoteNotificationPayload:(NSDictionary *)payload {
    return payload[@"fb_push_payload"][@"notificationId"];
}

+ (void)logCardOpenWithCampaignIdentifier:(nullable NSString *)identifier {
//    if (!identifier) {
//        return;
//    }
//    [self _logAppEventWithName:@"fb_mobile_push_opened" campaignIdentifier:identifier];
    NSNumber *notId = [NSNumber numberWithLongLong:[identifier longLongValue]];
    [[QGSdk getSharedInstance] logEvent:@"qg_inapp_toggled" withParameters:@{@"state" : @"open", @"notificationId" : notId}];
}

+ (void)logButtonAction:(QGFBNCardButtonAction)action forCardWithCampaignIdentifier:(nullable NSString *)identifier {
    //    NSString *eventName = [self _appEventNameForButtonAction:action];
    //    [self _logAppEventWithName:eventName campaignIdentifier:identifier];
    NSNumber *notId = [NSNumber numberWithLongLong:[identifier longLongValue]];
    [[QGSdk getSharedInstance] logEvent:@"qg_inapp_toggled" withParameters:@{@"state" : @"close", @"notificationId" : notId}];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

+ (void)_logAppEventWithName:(NSString *)name campaignIdentifier:(NSString *)identifier {
    if (!identifier) {
        return;
    }

//    [[QGSdk getSharedInstance] logEvent:name withParameters:@{ @"notificationId" : identifier }];
}

+ (nullable NSString *)_appEventNameForButtonAction:(QGFBNCardButtonAction)action {
    switch (action) {
        case QGFBNCardButtonActionPrimary:
            return @"qg_mobile_push_card_action_primary";
        case QGFBNCardButtonActionSecondary:
            return @"qg_mobile_push_card_action_secondary";
        case QGFBNCardButtonActionDismiss:
            return @"qg_mobile_push_card_action_dismiss";
        case QGFBNCardButtonActionBackgroundTouch:
            return @"qg_mobile_push_card_action_background_touched";
        default:break;
    }
    return nil;
}

@end

NS_ASSUME_NONNULL_END
