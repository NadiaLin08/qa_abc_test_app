// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardHeroConfiguration.h"

#import "QGFBNAssetContentCache.h"
#import "QGFBNAssetsController.h"
#import "QGFBNCardViewUtilities.h"

const CGFloat QGFBNCardHeroHeightUnspecified = CGFLOAT_MAX;

NS_ASSUME_NONNULL_BEGIN

@implementation QGFBNCardHeroConfiguration

///--------------------------------------
#pragma mark - Init
///--------------------------------------

- (instancetype)initFromDictionary:(NSDictionary<NSString *, id> *)dictionary
                    withBackground:(nullable id <QGFBNAsset>)background {
    self = [super init];
    if (!self) return self;

    NSNumber *height = dictionary[@"height"] ?: @(QGFBNCardHeroHeightUnspecified);
    _height = QGFBNCGFloatFromNumber(height);
    _background = background;

    _content = [QGFBNCardTextContent contentFromDictionary:dictionary[@"content"]];
    _contentVerticalAlignment = QGFBNCardContentVerticalAlignmentFromString(dictionary[@"contentAlign"]);

    return self;
}

+ (void)loadFromDictionary:(nullable NSDictionary<NSString *,id> *)dictionary
          assetsController:(QGFBNAssetsController *)controller
                completion:(void (^)(QGFBNCardHeroConfiguration * _Nullable))completion {
    if (!dictionary) {
        completion(nil);
        return;
    }
    [controller loadAssetFromDictionary:dictionary[@"background"] completion:^(id<QGFBNAsset> _Nullable asset) {
        QGFBNCardHeroConfiguration *configuration = [[self alloc] initFromDictionary:dictionary withBackground:asset];
        completion(configuration);
    }];
}

@end

NS_ASSUME_NONNULL_END
