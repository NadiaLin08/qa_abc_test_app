// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardActionsConfiguration.h"

#import "QGFBNAssetsController.h"
#import "QGFBNCardActionConfiguration.h"
#import "QGFBNCardViewUtilities.h"

NS_ASSUME_NONNULL_BEGIN

@implementation QGFBNCardActionsConfiguration

///--------------------------------------
#pragma mark - Init
///--------------------------------------

- (instancetype)initFromDictionary:(NSDictionary<NSString *, id> *)dictionary
                    withBackground:(nullable id<QGFBNAsset>)background {
    self = [super init];
    if (!self) return self;

    _style = QGFBNCardActionsStyleFromString(dictionary[@"style"]);
    _layoutStyle = QGFBNCardActionsLayoutStyleFromString(dictionary[@"layoutStyle"]);

    _background = background;

    NSNumber *height = dictionary[@"height"] ?: @(44.0); // Defaults to 44.0
    _height = QGFBNCGFloatFromNumber(height);
    _topInset = QGFBNCGFloatFromNumber(dictionary[@"topInset"]); // Default to 0
    _contentInset = QGFBNCGFloatFromNumber(dictionary[@"contentInset"]); // Defaults to 0
    _cornerRadius = QGFBNCGFloatFromNumber(dictionary[@"cornerRadius"]); // Defaults to 0

    NSArray<NSDictionary *> *rawActions = dictionary[@"actions"];
    NSMutableArray<QGFBNCardActionConfiguration *> *actions = [NSMutableArray arrayWithCapacity:rawActions.count];
    for (NSDictionary *rawAction in rawActions) {
        QGFBNCardActionConfiguration *action = [QGFBNCardActionConfiguration configurationFromDictionary:rawAction];
        [actions addObject:action];
    }
    _actions = [actions copy];

    return self;
}

+ (void)loadFromDictionary:(nullable NSDictionary<NSString *,id> *)dictionary
          assetsController:(QGFBNAssetsController *)controller
                completion:(void (^)(QGFBNCardActionsConfiguration * _Nullable configuration))completion {
    if (!dictionary) {
        completion(nil);
        return;
    }
    [controller loadAssetFromDictionary:dictionary[@"background"] completion:^(id<QGFBNAsset> _Nullable asset) {
        QGFBNCardActionsConfiguration *configuration = [[self alloc] initFromDictionary:dictionary withBackground:asset];
        completion(configuration);
    }];
}

@end

NS_ASSUME_NONNULL_END
