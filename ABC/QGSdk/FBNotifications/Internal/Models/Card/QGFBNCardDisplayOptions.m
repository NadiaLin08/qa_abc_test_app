// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardDisplayOptions.h"

#import "QGFBNCardColor.h"
#import "QGFBNCardViewUtilities.h"

NS_ASSUME_NONNULL_BEGIN

@implementation QGFBNCardDisplayOptions

- (instancetype)initFromDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (!self) return nil;

    _size = QGFBNCardSizeFromString(dictionary[@"size"]);
    _cornerRadius = QGFBNCGFloatFromNumber(dictionary[@"cornerRadius"]); // Defaults to 0
    _contentInset = QGFBNCGFloatFromNumber(dictionary[@"contentInset"] ?: @(10.0)); // Defaults to 10.0

    _backdropColor = QGFBNCardColorFromRGBAHex(dictionary[@"backdropColor"]);
    _dismissButtonColor = QGFBNCardColorFromRGBAHex(dictionary[@"dismissColor"]) ?: [UIColor blackColor];

    return self;
}

+ (instancetype)displayOptionsFromDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initFromDictionary:dictionary];
}

@end

NS_ASSUME_NONNULL_END
