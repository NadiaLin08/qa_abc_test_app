// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import <UIKit/UIKit.h>

#import "QGFBNCardPayload.h"

@protocol QGFBNAsset;
@protocol QGFBNAssetController;
@class QGFBNAssetContentCache;

NS_ASSUME_NONNULL_BEGIN

@interface QGFBNAssetsController : NSObject

///--------------------------------------
#pragma mark - Asset Controllers
///--------------------------------------

- (void)registerAssetController:(id<QGFBNAssetController>)controller forAssetType:(NSString *)type;
- (nullable id<QGFBNAssetController>)assetControllerForAssetType:(NSString *)type;

///--------------------------------------
#pragma mark - Assets
///--------------------------------------

- (void)loadAssetFromDictionary:(NSDictionary *)dictionary completion:(nonnull void (^)(id<QGFBNAsset> _Nullable asset))completion;
- (nullable UIView *)viewForAsset:(id<QGFBNAsset>)asset;

///--------------------------------------
#pragma mark - Cache
///--------------------------------------

- (void)cacheAssetContentForCardPayload:(QGFBNCardPayload *)payload completion:(dispatch_block_t)completion;
- (void)clearAssetContentCacheForCardPayload:(QGFBNCardPayload *)payload;
- (BOOL)hasCachedContentForCardPayload:(QGFBNCardPayload *)payload;

@end

NS_ASSUME_NONNULL_END
