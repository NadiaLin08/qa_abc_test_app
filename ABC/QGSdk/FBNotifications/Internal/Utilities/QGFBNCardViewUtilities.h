// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

///--------------------------------------
#pragma mark - CGFloat
///--------------------------------------

extern CGFloat QGFBNCGFloatFromNumber(NSNumber *_Nullable number);
extern CGFloat QGFBNCGFloatRound(CGFloat number, NSRoundingMode roundingMode);

///--------------------------------------
#pragma mark - CGRect
///--------------------------------------

extern CGRect QGFBNRectMakeWithOriginSize(CGPoint origin, CGSize size);
extern CGRect QGFBNRectMakeWithSizeCenteredInRect(CGSize size, CGRect rect);

///--------------------------------------
#pragma mark - CGSize
///--------------------------------------

extern CGSize QGFBNSizeMin(CGSize size1, CGSize size2);
extern CGSize QGFBNSizeMax(CGSize size1, CGSize size2);
extern CGFloat QGFBNAspectFillScaleThatFits(CGSize size, CGSize fitSize);

///--------------------------------------
#pragma mark - Screen Scaling
///--------------------------------------

extern CGRect QGFBNRectAdjustToScreenScale(CGRect rect, NSRoundingMode roundingMode);
extern CGSize QGFBNSizeAdjustToScreenScale(CGSize size, NSRoundingMode roundingMode);
extern CGFloat QGFBNFloatAdjustToScreenScale(CGFloat value, NSRoundingMode roundingMode);

///--------------------------------------
#pragma mark - Top Most View Controller
///--------------------------------------

extern UIViewController *_Nullable QGFBNApplicationTopMostViewController(void);

NS_ASSUME_NONNULL_END
