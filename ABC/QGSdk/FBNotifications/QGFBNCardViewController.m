// Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
//
// You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
// copy, modify, and distribute this software in source code or binary form for use
// in connection with the web services and APIs provided by Facebook.
//
// As with any software that integrates with the Facebook platform, your use of
// this software is subject to the Facebook Developer Principles and Policies
// [http://developers.facebook.com/policy/]. This copyright notice shall be
// included in all copies or substantial portions of the software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "QGFBNCardViewController.h"
#import "QGFBNCardViewController_Internal.h"

#import "QGFBNAssetsController.h"
#import "QGFBNAssetContentCache.h"
#import "QGFBNCardDisplayOptions.h"
#import "QGFBNCardHeroView.h"
#import "QGFBNCardBodyView.h"
#import "QGFBNCardActionsView.h"
#import "QGFBNCardDismissButton.h"
#import "QGFBNCardViewUtilities.h"
#import "QGFBNCardConfiguration.h"
#import "QGFBNCardHeroConfiguration.h"
#import "QGFBNCardBodyConfiguration.h"
#import "QGFBNCardActionsConfiguration.h"
#import "QGFBNCardActionConfiguration.h"
#import "QGFBNCardAppEventsLogger.h"
#import "QGFBNCardColor.h"

@interface QGFBNCardViewController () <QGFBNCardActionsViewDelegate>

@property (nullable, nonatomic, copy, readonly) NSString *campaignIdentifier;
@property (nonatomic, strong, readonly) QGFBNAssetsController *assetsController;

@property (nonatomic, strong, readonly) QGFBNCardDisplayOptions *cardDisplayOptions;
@property (nullable, nonatomic, strong) QGFBNCardConfiguration *configuration;

@property (nonatomic, strong) UIView *contentView;
@property (nullable, nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;
@property (nullable, nonatomic, strong) QGFBNCardHeroView *heroView;
@property (nullable, nonatomic, strong) QGFBNCardBodyView *bodyView;
@property (nullable, nonatomic, strong) QGFBNCardActionsView *actionsView;
@property (nullable, nonatomic, strong) QGFBNCardDismissButton *dismissButton;

@end

@implementation QGFBNCardViewController

///--------------------------------------
#pragma mark - Init
///--------------------------------------

- (instancetype)initWithPushCardPayload:(QGFBNCardPayload *)payload
                     campaignIdentifier:(nullable NSString *)campaignIdentifier
                       assetsController:(QGFBNAssetsController *)assetsController {
    self = [super initWithNibName:nil bundle:nil];
    if (!self) return self;

    _payload = [payload copy];
    _cardDisplayOptions = [QGFBNCardDisplayOptions displayOptionsFromDictionary:payload];
    _campaignIdentifier = [campaignIdentifier copy];
    _assetsController = assetsController;

    [self _reloadConfiguration];

    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

    return self;
}

- (void)dealloc {
    [self.dismissButton removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

///--------------------------------------
#pragma mark - View
///--------------------------------------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        [self _logPushOpen];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(_applicationWillEnterForeground)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
    }
}

///--------------------------------------
#pragma mark - Configuration
///--------------------------------------

- (void)_reloadConfiguration {
    //if ([self.assetsController hasCachedContentForCardPayload:self.payload]) {
        [QGFBNCardConfiguration loadFromDictionary:self.payload
                              withDisplayOptions:self.cardDisplayOptions
                                assetsController:self.assetsController
                                      completion:^(QGFBNCardConfiguration * _Nullable configuration) {
                                          dispatch_block_t reloadBlock = ^{
                                              self.configuration = configuration;
                                              if ([self isViewLoaded]) {
                                                  [self _reloadSubviews];
                                              }
                                          };
                                          if ([NSThread isMainThread]) {
                                              reloadBlock();
                                          } else {
                                              dispatch_async(dispatch_get_main_queue(), reloadBlock);
                                          }
                                      }];
//    } else {
//        __weak typeof(self) wself = self;
//        [self.assetsController cacheAssetContentForCardPayload:self.payload completion:^{
//            [wself _reloadConfiguration];
//        }];
//    }
}

///--------------------------------------
#pragma mark - Subviews
///--------------------------------------

- (void)_reloadSubviews {
    self.view.backgroundColor = self.cardDisplayOptions.backdropColor;

    self.contentView = [[UIView alloc] initWithFrame:CGRectZero];
    self.contentView.layer.cornerRadius = self.cardDisplayOptions.cornerRadius;
    self.contentView.clipsToBounds = YES;
    [self.view addSubview:self.contentView];

    if (self.configuration) {
        [self.loadingIndicatorView stopAnimating];
        [self.loadingIndicatorView removeFromSuperview];
        self.loadingIndicatorView = nil;

        [self _reloadContentViews];
    } else {
        QGFBNCardContrastColor contrastColor = QGFBNCardContrastColorForColor(self.view.backgroundColor);
        UIActivityIndicatorViewStyle indicatorViewStyle = (contrastColor == QGFBNCardContrastColorBlack ?
                                                           UIActivityIndicatorViewStyleGray :
                                                           UIActivityIndicatorViewStyleWhite);
        self.loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:indicatorViewStyle];
        [self.view addSubview:self.loadingIndicatorView];
        [self.loadingIndicatorView startAnimating];
    }
}

- (void)_reloadContentViews {
    if (self.configuration.heroConfiguration) {
        self.heroView = [[QGFBNCardHeroView alloc] initWithConfiguration:self.configuration.heroConfiguration
                                                      assetsController:self.assetsController
                                                          contentInset:self.cardDisplayOptions.contentInset];
        [self.contentView addSubview:self.heroView];
    }
    if (self.configuration.bodyConfiguration) {
        self.bodyView = [[QGFBNCardBodyView alloc] initWithConfiguration:self.configuration.bodyConfiguration
                                                      assetsController:self.assetsController
                                                          contentInset:self.cardDisplayOptions.contentInset];
        [self.contentView addSubview:self.bodyView];
    }
    if (self.configuration.actionsConfiguration) {
        self.actionsView = [[QGFBNCardActionsView alloc] initWithConfiguration:self.configuration.actionsConfiguration
                                                            assetsController:self.assetsController
                                                                    delegate:self];
        switch (self.configuration.actionsConfiguration.style) {
            case QGFBNCardActionsStyleAttached:
                [self.contentView addSubview:self.actionsView];
                break;
            case QGFBNCardActionsStyleDetached:
                [self.view addSubview:self.actionsView];
                break;
            default:break;
        }
    }
    [self _reloadDismissButton];
}

- (void)_reloadDismissButton {
    BOOL showsDismissButton = YES;
    for (QGFBNCardActionConfiguration *action in self.configuration.actionsConfiguration.actions) {
        if (!action.actionURL) {
            showsDismissButton = NO;
            break;
        }
    }
    if (showsDismissButton) {
        self.dismissButton = [[QGFBNCardDismissButton alloc] initWithFrame:CGRectZero];
        self.dismissButton.imageColor = self.cardDisplayOptions.dismissButtonColor;
        [self.dismissButton addTarget:self action:@selector(_dismissButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.dismissButton];
    }
}

///--------------------------------------
#pragma mark - UIViewController
///--------------------------------------

- (void)loadView {
    [super loadView];

    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self _reloadSubviews];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    CGRect bounds = self.view.bounds;
    if (self.cardDisplayOptions.size == QGFBNCardSizeLarge) {
        bounds.size.height -= self.topLayoutGuide.length;
        bounds.origin.y += self.topLayoutGuide.length;
    }

    CGSize availableSize = QGFBNCardLayoutSizeThatFits(self.cardDisplayOptions.size, bounds.size);

    const CGSize bodySize = CGSizeMake(availableSize.width, [self.bodyView sizeThatFits:availableSize].height);
    availableSize.height -= bodySize.height;

    const CGSize actionsSize = CGSizeMake(availableSize.width, [self.actionsView sizeThatFits:availableSize].height);
    availableSize.height -= actionsSize.height;

    CGSize heroSize = availableSize;
    if (self.configuration.heroConfiguration.height == QGFBNCardHeroHeightUnspecified) {
        heroSize.height = [self.heroView sizeThatFits:availableSize].height;
    } else {
        heroSize.height *= self.configuration.heroConfiguration.height;
    }
    heroSize = QGFBNSizeAdjustToScreenScale(heroSize, NSRoundUp);

    CGSize contentSize = CGSizeMake(availableSize.width, bodySize.height + heroSize.height);

    const CGRect heroFrame = QGFBNRectMakeWithOriginSize(CGPointZero, heroSize);
    const CGRect bodyFrame = QGFBNRectMakeWithOriginSize(CGPointMake(CGRectGetMinX(heroFrame), CGRectGetMaxY(heroFrame)), bodySize);
    CGRect actionsFrame = QGFBNRectMakeWithOriginSize(CGPointZero, actionsSize);
    CGRect contentFrame = CGRectZero;
    switch (self.configuration.actionsConfiguration.style) {
        case QGFBNCardActionsStyleAttached: {
            actionsFrame.origin = CGPointMake(CGRectGetMinX(heroFrame), CGRectGetMaxY(bodyFrame));
            contentSize.height += actionsSize.height;
            contentFrame = QGFBNRectMakeWithSizeCenteredInRect(contentSize, bounds);
        }
            break;
        case QGFBNCardActionsStyleDetached: {
            CGPoint contentOrigin = QGFBNRectMakeWithSizeCenteredInRect(CGSizeMake(contentSize.width, contentSize.height + actionsSize.height),
                                                                     bounds).origin;
            contentFrame = QGFBNRectMakeWithOriginSize(contentOrigin, contentSize);
            actionsFrame.origin = CGPointMake(CGRectGetMinX(contentFrame), CGRectGetMaxY(contentFrame));
        }
            break;
        default:break;
    }
    CGRect dismissButtonFrame = QGFBNRectMakeWithOriginSize(CGPointZero, [self.dismissButton sizeThatFits:bounds.size]);
    dismissButtonFrame.origin.x = CGRectGetMaxX(contentFrame) - CGRectGetWidth(dismissButtonFrame) - self.cardDisplayOptions.contentInset;
    dismissButtonFrame.origin.y = CGRectGetMinY(contentFrame) + self.cardDisplayOptions.contentInset;

    self.contentView.frame = contentFrame;
    self.heroView.frame = heroFrame;
    self.bodyView.frame = bodyFrame;
    self.actionsView.frame = actionsFrame;
    self.dismissButton.frame = dismissButtonFrame;
    self.loadingIndicatorView.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}

///--------------------------------------
#pragma mark - Application State Changes
///--------------------------------------

- (void)_applicationWillEnterForeground {
    [self _logPushOpen];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

///--------------------------------------
#pragma mark - Logging Events
///--------------------------------------

- (void)_logPushOpen {
    [QGFBNCardAppEventsLogger logCardOpenWithCampaignIdentifier:self.campaignIdentifier];
}

///--------------------------------------
#pragma mark - Dismiss
///--------------------------------------
//changes made by shiv
- (void)_dismissFromButtonAction:(QGFBNCardButtonAction)action withOpenURL:(nullable NSURL *)url {
    [QGFBNCardAppEventsLogger logButtonAction:action forCardWithCampaignIdentifier:self.campaignIdentifier];

    id<QGFBNCardViewControllerDelegate> delegate = self.delegate;
    if (url) {
        if ([delegate respondsToSelector:@selector(pushCardViewController:willDismissWithOpenURL:)]) {
            [delegate pushCardViewController:self willDismissWithOpenURL:url];
        }
        //[[UIApplication sharedApplication] openURL:url];
    } else {
        if ([delegate respondsToSelector:@selector(pushCardViewControllerWillDismiss:)]) {
            [delegate pushCardViewControllerWillDismiss:self];
        }
    }
    //[self.assetsController clearAssetContentCacheForCardPayload:self.payload];

    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([delegate respondsToSelector:@selector(pushCardViewControllerDidDismissWithOpenURL:)]) {
        [delegate pushCardViewControllerDidDismissWithOpenURL:url];
    }
}

///--------------------------------------
#pragma mark - QGFBNCardActionsView
///--------------------------------------

- (void)actionsView:(QGFBNCardActionsView *)view didPerformButtonAction:(QGFBNCardButtonAction)action withOpenURL:(nullable NSURL *)url {
    [self _dismissFromButtonAction:action withOpenURL:url];
}

///--------------------------------------
#pragma mark - Dismiss Button
///--------------------------------------

- (void)_dismissButtonAction {
    [self _dismissFromButtonAction:QGFBNCardButtonActionDismiss withOpenURL:nil];
}

///--------------------------------------
#pragma mark - Touch View to Dismiss
///--------------------------------------
//added by shiv
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch= [touches anyObject];
    if ([touch view] == self.view) {
        [self _dismissFromButtonAction:QGFBNCardButtonActionBackgroundTouch withOpenURL:nil];
    }
}

@end
