//
//  QGNotificationEvents.m
//  QGNotificationSdk
//
//  Created by Shiv on 08/08/16.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import "QGNotificationEvents.h"

#define QG_EXT_NOTIFICATION_SDK_VERSION_NO          @"4.5.0"
#define QG_EXT_LAST_CLICKED_NOTIFICATION_ID         @"lastClkdNotId"
#define QG_EXT_LAST_CLICKED_NOTIFICATION_TIME       @"qgLastClkdNotTime"
#define QG_EXT_NOTIFICATION_ID                      @"notificationId"
#define QG_EXT_CLICK_ATTRIBUTION_WINDOW             @"qgClickAttributionWindow"
#define QG_EXT_VIEW_THROUGH_ATTRIBUTION_WINDOW      @"qgViewThroughAttributionWindow"
#define QG_EXT_APP_ID                               @"qgAppId"
#define QG_EXT_APP_SECRET                           @"qgAppSecret"
#define QG_EXT_USER_ID                              @"qgUserId"
#define QG_EXT_LAST_VIEW_THROUGH_NOTIFICATION_ID    @"qg_last_view_through_notification_id"
#define QG_EXT_LAST_VIEW_THROUGH_TIME               @"qg_last_view_through_time"
#define QG_EXT_CAMPAIGN_ID                          @"qg"
#define QG_EXT_NOTIF_ID                             @"nid"

#define QG_EXT_FORCE_TOUCH_CAPABILITY_STATUS        @"qg_force_touch_capability_status"
#define QG_EXT_RICH_PUSH_SUPPORTED_STATUS           @"qg_rich_push_notification_supported_status"

#define QG_EXT_BATCHED_URL                          @"https://api.quantumgraph.com/qga/"

#define QGW_EXT_CURRENT_TIME          [[NSDate date] timeIntervalSince1970]

@implementation QGNotificationEvents

static QGNotificationEvents *instance_ = nil;
static NSUserDefaults *defaults_ = nil;

+ (QGNotificationEvents *)instance {
    return instance_;
}

+ (QGNotificationEvents *)instanceWithAppGroup:(NSString *)appGroup {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[super alloc] initWithAppGroup:appGroup];
    });
    return instance_;
}

- (instancetype)initWithAppGroup:(NSString *)appGroup {
    if (self = [self init]) {
        self.appGroup = appGroup;
        [self setUserDefaultsWithAppGroup:appGroup];
    }
    return self;
}

///------------------------------
#pragma mark - Event Logging
///------------------------------

- (NSDictionary *)getBasicDictionary {
    NSDictionary *basicDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [defaults_ objectForKey:QG_EXT_USER_ID], @"userId",
                                     @"ios", @"device",
                                     QG_EXT_NOTIFICATION_SDK_VERSION_NO, @"notifSdkVersion",
                                     nil];
    return basicDictionary;
}

- (void)sendDictionary:(NSDictionary *)dictionary {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString *appSecret = [defaults_ objectForKey:QG_EXT_APP_SECRET];
    NSURL *URL = [NSURL URLWithString:[QG_EXT_BATCHED_URL stringByAppendingFormat:@"%@/data/", [defaults_ objectForKey:QG_EXT_APP_ID]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"appliction/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:appSecret forHTTPHeaderField:@"appSecret"];
    [request setHTTPBody: jsonData];
    [request setTimeoutInterval:60];
    
    NSURLSessionDataTask *task = [[self logSession] dataTaskWithRequest:request
                                                      completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      // ...
                                  }];
    [task resume];
}

- (void)extensionLogEvent:(NSString *)name withParameters:(NSDictionary *)parameters withValueToSum:(NSNumber *)valueToSum {
    
    if (![self isValidStr:name]) {
        NSLog(@"warning: invalid event name passed, (%@)", name);
        return;
    }
    
    NSMutableDictionary *baseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[self getBasicDictionary]];
    NSMutableDictionary *infoDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys: name, @"eventName", nil];
    if (parameters != nil) {
        [infoDictionary setObject:parameters forKey:@"parameters"];
    }
    if (valueToSum != nil) {
        [infoDictionary setObject:valueToSum forKey:@"vts"];
    }
    if ([self isClickThroughAttribution]) {
        [infoDictionary setObject:[self getUserDefaultsForKey:QG_EXT_LAST_CLICKED_NOTIFICATION_ID] forKey:QG_EXT_LAST_CLICKED_NOTIFICATION_ID];
    }
    if (![infoDictionary objectForKey:QG_EXT_NOTIFICATION_ID]
        && [self isViewThroughAttribution]) {
        [infoDictionary setObject:[self getUserDefaultsForKey:QG_EXT_LAST_VIEW_THROUGH_NOTIFICATION_ID] forKey:QG_EXT_NOTIFICATION_ID];
    }
    NSArray *events = [NSArray arrayWithObjects:infoDictionary, nil];
    [baseDictionary setObject:events forKey:@"events"];
    
    [self sendDictionary:baseDictionary];
}

///-------------------------------------------
#pragma mark - Standard Event Tracking
///-------------------------------------------

- (void)trackNotificationReceived:(NSDictionary *)userInfo {
    if ([userInfo count] && ![userInfo isKindOfClass:[NSNull class]] && [userInfo objectForKey:QG_EXT_NOTIF_ID]) {
        NSNumber *notificationId = [self getNotificationIdFor:userInfo];
        NSDictionary *param = @{QG_EXT_NOTIFICATION_ID:notificationId};
        [self extensionLogEvent:@"notification_received" withParameters:param withValueToSum:nil];
    }
}

// set view through only if the notification is displayed but not received
- (void)trackNotificationDisplayed:(NSDictionary *)userInfo {
    if ([userInfo count] && ![userInfo isKindOfClass:[NSNull class]] && [userInfo objectForKey:QG_EXT_NOTIF_ID]) {
        NSNumber *notificationId = [self getNotificationIdFor:userInfo];
        NSDictionary *param = @{QG_EXT_NOTIFICATION_ID:notificationId};
        //save last shown notification id & time
        [self setViewThroughForNotificationId:notificationId];
        [self extensionLogEvent:@"notification_displayed" withParameters:param withValueToSum:nil];
    }
}

// track qg_carousel_clicked with item details
- (void)trackCarouselItemClicked:(NSDictionary *)userInfo withItem:(NSDictionary *)item {
    if ([userInfo count] && ![userInfo isKindOfClass:[NSNull class]] && [userInfo objectForKey:QG_EXT_NOTIF_ID]) {
        NSNumber *notificationId = [self getNotificationIdFor:userInfo];
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:item];
        param[QG_EXT_NOTIFICATION_ID] = notificationId;
        [self extensionLogEvent:@"qg_carousel_clicked" withParameters:param withValueToSum:nil];
    }
}

- (void)trackNotificationBrowsed:(NSDictionary *)userInfo {
    if ([userInfo count] && ![userInfo isKindOfClass:[NSNull class]] && [userInfo objectForKey:QG_EXT_NOTIF_ID]) {
        NSNumber *notificationId = [self getNotificationIdFor:userInfo];
        NSDictionary *param = @{QG_EXT_NOTIFICATION_ID:notificationId};
        [self extensionLogEvent:@"notification_browsed" withParameters:param withValueToSum:nil];
    }
}

- (void)trackRichPushOpened:(NSDictionary *)userInfo {
    if ([userInfo count] && ![userInfo isKindOfClass:[NSNull class]] && [userInfo objectForKey:QG_EXT_NOTIF_ID]) {
        NSNumber *notificationId = [self getNotificationIdFor:userInfo];
        NSDictionary *param = @{QG_EXT_NOTIFICATION_ID:notificationId};
        [self extensionLogEvent:@"qg_rich_push_opened" withParameters:param withValueToSum:nil];
    }
}

- (NSNumber *)getNotificationIdFor:(NSDictionary *)userInfo {
    return [NSNumber numberWithInteger:[[userInfo objectForKey:QG_EXT_NOTIF_ID] integerValue]];
}

///-------------------------------------------
#pragma mark - Click Through & View Through
///-------------------------------------------

//view through attribution for notification
- (void)setViewThroughForNotificationId:(NSNumber *)notificationId {
    [self saveUserDefaultsWithValue:notificationId Key:QG_EXT_LAST_VIEW_THROUGH_NOTIFICATION_ID];
    [self saveUserDefaultsWithValue:QGEXTCurrentTime() Key:QG_EXT_LAST_VIEW_THROUGH_TIME];
}

//click through attribution for click on notification
- (void)setClickThroughForNotificationId:(NSNumber *)notificationId {
    [self saveUserDefaultsWithValue:notificationId Key:QG_EXT_LAST_CLICKED_NOTIFICATION_ID];
    [self saveUserDefaultsWithValue:QGEXTCurrentTime() Key:QG_EXT_LAST_CLICKED_NOTIFICATION_TIME];
}

- (NSInteger)getClickAttributionWindow {
    if ([self isUserDefaultsForKey:QG_EXT_CLICK_ATTRIBUTION_WINDOW]) {
        return [[self getUserDefaultsForKey:QG_EXT_CLICK_ATTRIBUTION_WINDOW] integerValue];
    }
    return 86400;   //default to 24hrs(in second)
}

//check if last clicked notif. time is less than 24 hrs
- (BOOL)isClickThroughAttribution {
    if ([self isUserDefaultsForKey:QG_EXT_LAST_CLICKED_NOTIFICATION_TIME]) {
        NSDate *lastClickedNotificationTime = [self getUserDefaultsForKey:QG_EXT_LAST_CLICKED_NOTIFICATION_TIME];
        NSTimeInterval secs = [QGEXTCurrentTime() timeIntervalSinceDate:lastClickedNotificationTime];
        if (secs <= [self getClickAttributionWindow]) {
            return YES;
        }else {
            [self removeUserDefaultsForKey:QG_EXT_LAST_CLICKED_NOTIFICATION_TIME];
            return NO;
        }
    }
    return NO;
}

- (NSInteger)getViewThroughAttributionWindow {
    if ([self isUserDefaultsForKey:QG_EXT_VIEW_THROUGH_ATTRIBUTION_WINDOW]) {
        return [[self getUserDefaultsForKey:QG_EXT_VIEW_THROUGH_ATTRIBUTION_WINDOW] integerValue];
    }
    return 3600;   //default to 1hr(in second)
}

- (BOOL)isViewThroughAttribution {
    if ([self isUserDefaultsForKey:QG_EXT_LAST_VIEW_THROUGH_TIME]) {
        NSDate *viewThroughTime = [self getUserDefaultsForKey:QG_EXT_LAST_VIEW_THROUGH_TIME];
        NSTimeInterval secs = [QGEXTCurrentTime() timeIntervalSinceDate:viewThroughTime];
        if (secs <= [self getViewThroughAttributionWindow]) {
            return YES;
        } else {
            [self removeUserDefaultsForKey:QG_EXT_LAST_VIEW_THROUGH_TIME];
            return NO;
        }
    }
    return NO;
}

NSDate * QGEXTCurrentTime() {
    return [NSDate date];
}

- (NSURLSession *)logSession {
    static NSURLSession *session_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
        conf.HTTPMaximumConnectionsPerHost = 5;
        session_ = [NSURLSession sessionWithConfiguration:conf];
    });
    return session_;
}

///-----------------------------
#pragma mark - UserDefaults
///-----------------------------

- (void)setUserDefaultsWithAppGroup:(NSString *)appGroup {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaults_ = [[NSUserDefaults alloc] initWithSuiteName:appGroup];
    });
}

//save userDefaults with a key and value
- (void)saveUserDefaultsWithValue:(id)value Key:(NSString*)key {
    [defaults_ setObject:value forKey:key];
    [defaults_ synchronize];
}

- (id)getUserDefaultsForKey:(NSString *)key {
    return [defaults_ objectForKey:key];
}

//check for a key in the userDefaults
- (BOOL)isUserDefaultsForKey:(NSString *)key {
    return ([defaults_ objectForKey:key] != nil) ? YES : NO;
}

- (void)removeUserDefaultsForKey:(NSString *)key {
    [defaults_ removeObjectForKey:key];
    [defaults_ synchronize];
}

- (BOOL)hasForceTouchCapability {
    BOOL hasForceTouch = [[self getUserDefaultsForKey:QG_EXT_FORCE_TOUCH_CAPABILITY_STATUS] boolValue];
    return hasForceTouch;
}

//used to show rich push carousel/slider
- (BOOL)hasRichPushSupport {
    BOOL isSupported = [[self getUserDefaultsForKey:QG_EXT_RICH_PUSH_SUPPORTED_STATUS] boolValue];
    return isSupported;
}

- (BOOL)isValidStr:(NSString *)str {
    BOOL valid = YES;
    
    if (![str isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    str = [str stringByTrimmingCharactersInSet:
           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ( (str == nil)
        || ([str length] == 0)
        || ([str isKindOfClass:[NSNull class]])
        || ([str isEqualToString:@""])) {
        
        valid = NO;
    }
    
    return valid;
}

@end
