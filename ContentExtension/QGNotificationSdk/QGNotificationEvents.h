//
//  QGNotificationEvents.h
//  QGNotificationSdk
//
//  Created by Shiv on 08/08/16.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QGNotificationEvents : NSObject

@property (nonatomic, copy) NSString *appGroup;

+ (QGNotificationEvents *)instance;
+ (QGNotificationEvents *)instanceWithAppGroup:(NSString *)appGroup;

- (void)extensionLogEvent:(NSString *)name withParameters:(NSDictionary *)parameters withValueToSum:(NSNumber *)valueToSum;

- (void)trackNotificationReceived:(NSDictionary *)userInfo;
- (void)trackNotificationDisplayed:(NSDictionary *)userInfo;
- (void)trackCarouselItemClicked:(NSDictionary *)userInfo withItem:(NSDictionary *)item;
- (void)trackNotificationBrowsed:(NSDictionary *)userInfo;
- (void)trackRichPushOpened:(NSDictionary *)userInfo;

- (BOOL)hasForceTouchCapability;
- (BOOL)hasRichPushSupport;

- (void)saveUserDefaultsWithValue:(id)value Key:(NSString*)key;

@end
