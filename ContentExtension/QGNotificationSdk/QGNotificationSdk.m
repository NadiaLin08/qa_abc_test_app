//
//  QGNotificationSdk.m
//  QGNotificationSdk
//
//  Created by Shiv on 05/08/16.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import "QGNotificationSdk.h"
#import <CommonCrypto/CommonDigest.h>
#import "QGNotificationEvents.h"

#define QG_EXT_RICH_PUSH_DEEPLINK                   @"qg_rich_push_deeplink"
#define QG_EXT_CAROUSEL_ACTION_CATEGORY             @"QGCAROUSEL"
#define QG_EXT_CAROUSEL_ACTION_OPEN_APP             @"qg_rich_push_action_open_app"
#define QG_EXT_CAROUSEL_ACTION_NEXT                 @"qg_carousel_action_next"

@interface QGNotificationSdk() {
    float slider_width;
    float slider_height;
}

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
@property (nonatomic, strong) NSString *APP_GROUP;

@property (nonatomic, weak) iCarousel *carousel;
@property (nonatomic, copy) UNNotification *receivedNotification;
@property (nonatomic, assign) BOOL isSlider;
@property (nonatomic, assign) BOOL isCustom;

@end

@implementation QGNotificationSdk

static float CAROUSEL_CARD_WIDTH = 260;
static float CAROUSEL_CARD_HEIGHT = 330;

static QGNotificationSdk *instance_ = nil;

+ (QGNotificationSdk *)sharedInstanceWithAppGroup:(NSString *)appGroup {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        instance_ = [[super alloc] initWithAppGroup:appGroup];
    });
    return instance_;
}

- (instancetype)initWithAppGroup:(NSString *)appGroup {
    if (appGroup == nil || appGroup.length == 0) {
        NSLog(@"%@ warning empty app group", self);
    }
    if (self = [self init]) {
        _APP_GROUP = appGroup;
        [QGNotificationEvents instanceWithAppGroup:appGroup];
    }
    return self;
}

//-----------------------------------------
#pragma mark - Service Extension Code
//-----------------------------------------

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent *))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    NSMutableDictionary *userInfo = [request.content.userInfo mutableCopy];
    
    self.isCustom = false;
    if (userInfo[@"source"] && [userInfo[@"source"] isEqualToString:@"QG"] && userInfo[@"qgPush"]) {
        
        //track notification_received for Source QG
        [[QGNotificationEvents instance] trackNotificationReceived:userInfo];
        
        NSString *type = userInfo[@"qgPush"][@"type"];
        if ([type caseInsensitiveCompare:@"slider"] == NSOrderedSame || [type caseInsensitiveCompare:@"carousel"] == NSOrderedSame) {
            self.isCustom = true;
            NSArray *data = userInfo[@"qgPush"][@"custom"][@"data"];
            if (data.count == 0) {
                [self returnCallbackDidReceiveNotificationRequest:userInfo];
                return;
            }
            [self downloadResourceFor:data withCompletionHandler:^(BOOL finished){
                if (finished) {
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    NSMutableArray *editedData = [NSMutableArray array];
                    NSMutableArray <UNNotificationAttachment *> *attachments = [NSMutableArray array];
                    for (NSDictionary *dict in data) {
                        NSURL *filePath = [self getFilePathFor:dict[@"imageUrl"]];
                        if ([fileManager fileExistsAtPath:filePath.path]) {
                            if (attachments.count == 0) {
                                NSString *copiedPath = [self getCopiedFilePathFor:dict[@"imageUrl"]];
                                NSError *copyError = nil;
                                if (![fileManager fileExistsAtPath:copiedPath]) {
                                    if (![fileManager copyItemAtPath:filePath.path toPath:copiedPath error:&copyError]) {
                                        //NSLog(@"Copy error: %@", copyError);
                                    }
                                }
                                NSString *identifier = QGMD5HashFromString(dict[@"imageUrl"]);
                                UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:identifier URL:[NSURL fileURLWithPath:copiedPath] options:nil error:nil];
                                if (attachment) {
                                    [attachments addObject:attachment];
                                }
                            }
                            [editedData addObject:dict];
                        }
                    }
                    NSMutableDictionary *custom = [userInfo[@"qgPush"][@"custom"] mutableCopy];
                    custom[@"data"] = editedData;
                    NSMutableDictionary *qgpush = [userInfo[@"qgPush"] mutableCopy];
                    qgpush[@"custom"] = custom;
                    userInfo[@"qgPush"] = qgpush;
                    self.bestAttemptContent.userInfo = userInfo;
                    self.bestAttemptContent.attachments = attachments;
                    if ([[QGNotificationEvents instance] hasRichPushSupport]) {
                        if ([type caseInsensitiveCompare:@"slider"] == NSOrderedSame && editedData.count >=2 ) {
                            self.bestAttemptContent.categoryIdentifier = QG_EXT_CAROUSEL_ACTION_CATEGORY;
                        } else if ([type caseInsensitiveCompare:@"carousel"] == NSOrderedSame && editedData.count >= 3) {
                            self.bestAttemptContent.categoryIdentifier = QG_EXT_CAROUSEL_ACTION_CATEGORY;
                        } else {
                            self.bestAttemptContent.categoryIdentifier = @"qgna";
                        }
                    }
                }
                [self returnCallbackDidReceiveNotificationRequest:userInfo];
            }];
        } else if ([type caseInsensitiveCompare:@"basic"] == NSOrderedSame) {
            //qgPush type --- image/gif/audio/video
            if (userInfo[@"qgPush"][@"url"]) {
                NSString *url = userInfo[@"qgPush"][@"url"];
                NSArray *data = @[@{@"imageUrl": url}];
                [self downloadResourceFor:data withCompletionHandler:^(BOOL finished){
                    if (finished) {
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        NSURL *filePath = [self getFilePathFor:url];
                        if ([fileManager fileExistsAtPath:filePath.path]) {
                            NSString *identifier = QGMD5HashFromString(url);
                            NSError *error = nil;
                            UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:identifier URL:filePath options:nil error:&error];
                            if (error) {
                                NSLog(@"error creating attachment %@", error);
                            }
                            if (attachment) {
                                self.bestAttemptContent.attachments = @[attachment];
                            }
                        }
                        [self returnCallbackDidReceiveNotificationRequest:userInfo];
                    }
                }];
            } else {
                [self returnCallbackDidReceiveNotificationRequest:userInfo];
            }
        } else {
            [self returnCallbackDidReceiveNotificationRequest:userInfo];
        }
    } else {
        // this is not push from AIQUA
        self.contentHandler(self.bestAttemptContent);
    }
}


- (void)returnCallbackDidReceiveNotificationRequest:(NSDictionary *)userInfo {
    // log notification_displayed event
    [[QGNotificationEvents instance] trackNotificationDisplayed:userInfo];
    self.contentHandler(self.bestAttemptContent);
}


- (void)serviceExtensionTimeWillExpire {
    self.contentHandler(self.bestAttemptContent);
}

- (void)downloadResourceFor:(NSArray *)data withCompletionHandler:(void (^) (BOOL completed))completionHandler{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURLSession *session = [NSURLSession sharedSession];
    
    dispatch_group_t group = dispatch_group_create();
    
    for (NSDictionary *dict in data) {
        if ([fileManager fileExistsAtPath:[self getFilePathFor:dict[@"imageUrl"]].path]) {
            continue;
        }
        NSURL *imageUrl = [NSURL URLWithString:dict[@"imageUrl"]];
        dispatch_group_enter(group);
        NSURLSessionDownloadTask *task = [session downloadTaskWithURL:imageUrl completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error){
            if (!error) {
                NSString *urlString = response.URL.absoluteString;
                NSURL *savePath = [self getFilePathFor:urlString];
                if ([fileManager fileExistsAtPath:savePath.path]) {
                    [fileManager removeItemAtURL:savePath error:nil];
                }
                NSError *moveError = nil;
                if (![fileManager moveItemAtURL:location toURL:savePath error:&moveError]) {
                    //NSLog(@"moveItemAtURL failed: %@", moveError);
                }
            }
            dispatch_group_leave(group);
        }];
        [task resume];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(),^{
        completionHandler(YES);
    });
}

-(NSURL *)getFilePathFor:(NSString *)urlString {
    NSURL* path = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:_APP_GROUP];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURL *fileURL = [[path URLByAppendingPathComponent:QGMD5HashFromString(urlString)] URLByAppendingPathExtension:url.pathExtension];
    return fileURL;
}

- (NSString *)getCopiedFilePathFor:(NSString *)urlString {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *filePath = [[path stringByAppendingPathComponent:QGMD5HashFromString(urlString)] stringByAppendingPathExtension:url.pathExtension];
    
    return filePath;
}

NSString *QGMD5HashFromData(NSData *data) {
    unsigned char md[CC_MD5_DIGEST_LENGTH];
    CC_MD5_CTX ctx_val = { 0 };
    CC_MD5_CTX *ctx_ptr = &ctx_val;
    CC_MD5_Init(ctx_ptr);
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        CC_MD5_Update(ctx_ptr , bytes, (CC_LONG)byteRange.length);
    }];
    CC_MD5_Final(md, ctx_ptr);
    
    NSString *string = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                        md[0], md[1],
                        md[2], md[3],
                        md[4], md[5],
                        md[6], md[7],
                        md[8], md[9],
                        md[10], md[11],
                        md[12], md[13],
                        md[14], md[15]];
    return string;
}

NSString *QGMD5HashFromString(NSString *string) {
    return QGMD5HashFromData([string dataUsingEncoding:NSUTF8StringEncoding]);
}

//-----------------------------------------
#pragma mark - Content Extension Code
//-----------------------------------------

- (void)viewDidLoadWithCarousel:(iCarousel *)carousel {
    _carousel = carousel;
    _carousel.pagingEnabled = true;
    _isSlider = false;
}

- (void)didReceiveNotification:(UNNotification *)notification {
    self.receivedNotification = [notification copy];
    UNNotificationContent *content = notification.request.content;
    
    //track event qg_rich_push_opened
    [[QGNotificationEvents instance] trackRichPushOpened:content.userInfo];
    iCarouselType type = iCarouselTypeLinear;
    if ([content.userInfo objectForKey:@"qgPush"][@"custom"][@"carouselType"]) {
        NSString *carouselType = content.userInfo[@"qgPush"][@"custom"][@"carouselType"];
        if ([carouselType caseInsensitiveCompare:@"Slider"] == NSOrderedSame) {
            //type is linear by default
            //50 as margin horizontal
            slider_width = self.carousel.bounds.size.width - 40;
            self.isSlider = true;
            [self fixCarouselHeightForSlider];
        } else {
            self.isSlider = false;
            type = [self getCarouselType:carouselType];
            [self setHeightConstraint:CAROUSEL_CARD_HEIGHT + 5];
        }
    }
    _carousel.type = type;
    [_carousel reloadData];
}

- (void)didReceiveNotificationResponse:(UNNotificationResponse *)response withContext:(NSExtensionContext *)context completionHandler:(void (^)(UNNotificationContentExtensionResponseOption))completion {
    if ([response.actionIdentifier isEqualToString:QG_EXT_CAROUSEL_ACTION_NEXT]) {
        [_carousel scrollByNumberOfItems:1 duration:0.5];
        completion(UNNotificationContentExtensionResponseOptionDoNotDismiss);
        //track notification_browsed
        [[QGNotificationEvents instance] trackNotificationBrowsed:_receivedNotification.request.content.userInfo];
    } else {
        if ([response.actionIdentifier isEqualToString:QG_EXT_CAROUSEL_ACTION_OPEN_APP]) {
            UNNotificationContent *content = self.receivedNotification.request.content;
            NSDictionary *obj = content.userInfo[@"qgPush"][@"custom"][@"data"][_carousel.currentItemIndex];
            // track qg_carousel_clicked
            [[QGNotificationEvents instance] trackCarouselItemClicked:content.userInfo withItem:obj];
            if (obj[@"deepLink"]) {
                NSString *urlString = obj[@"deepLink"];
                [[QGNotificationEvents instance] saveUserDefaultsWithValue:urlString Key:QG_EXT_RICH_PUSH_DEEPLINK];
            }
        }
        completion(UNNotificationContentExtensionResponseOptionDismissAndForwardAction);
    }
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return [self getCarouselCount];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
//    if (carousel.type == iCarouselTypeLinear) {
//        view = nil;
//    }
    CGSize card_size = CGSizeMake(CAROUSEL_CARD_WIDTH, CAROUSEL_CARD_HEIGHT);
    if (_isSlider) {
        card_size = CGSizeMake(slider_width, slider_height);
    }
    
    if (view == nil || carousel.type == iCarouselTypeLinear)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, card_size.width, card_size.height)];
        view.contentMode = UIViewContentModeCenter;
        view.layer.cornerRadius = _isSlider ? 5 : 10;
        view.layer.masksToBounds = YES;
        view.backgroundColor = [UIColor whiteColor];
        
        NSDictionary *userInfo = self.receivedNotification.request.content.userInfo;
        NSDictionary *obj = userInfo[@"qgPush"][@"custom"][@"data"][index];
        CGRect imageFrame = CGRectMake(0, 0, card_size.width, card_size.height - 70);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
        if (userInfo[@"qgPush"][@"custom"][@"aspect"]) {
            NSString *aspect = userInfo[@"qgPush"][@"custom"][@"aspect"];
            imageView.contentMode = [self getContentMode:aspect];
        } else {
            if (_isSlider) {
                imageView.contentMode = UIViewContentModeScaleAspectFill;
            } else {
                imageView.contentMode = UIViewContentModeScaleAspectFit;
            }
        }
        imageView.clipsToBounds = true;
        
        NSString *urlString = obj[@"imageUrl"];
        
        if ([urlString rangeOfString:@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/slider_"].location != NSNotFound
            || [urlString rangeOfString:@"https://cdn.qgraph.io/img/574d03bff99b7e9109ec/carousel_"].location != NSNotFound) {
            NSURL *url = [NSURL URLWithString:urlString];
            NSString *imageName = [url.lastPathComponent componentsSeparatedByString:@"."][0];
            NSURL *imageUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"]];
            imageView.image = [UIImage imageWithContentsOfFile:imageUrl.path];
        } else {
            NSURL *imageUrl = [self getFilePathFor:obj[@"imageUrl"]];
            imageView.image = [UIImage imageWithContentsOfFile:imageUrl.path];
        }
        
        if (obj[@"title"]) {
            CGRect titleFrame = CGRectMake(20, imageFrame.size.height + 5, imageFrame.size.width - 40, 20);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleFrame];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
            titleLabel.text = obj[@"title"];
            [view addSubview:titleLabel];
        }
        if (obj[@"body"]) {
            CGRect bodyFrame = CGRectMake(20, imageFrame.size.height + 25, imageFrame.size.width - 40, 40);
            UILabel *bodyLabel = [[UILabel alloc] initWithFrame:bodyFrame];
            bodyLabel.text = obj[@"body"];
            bodyLabel.textAlignment = NSTextAlignmentCenter;
            bodyLabel.numberOfLines = 2;
            bodyLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
            [bodyLabel sizeToFit];
            CGRect frame = bodyLabel.frame;
            frame.size.width = card_size.width - 40;
            bodyLabel.frame = frame;
            [view addSubview:bodyLabel];
        }
        
        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(20, imageFrame.size.height, imageFrame.size.width - 40, 0.5)];
        separator.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.9];
        
        [view addSubview:imageView];
        [view addSubview:separator];
        view.clipsToBounds = true;
    }
    
    return view;
}

- (NSInteger)getCarouselCount {
    @try {
        NSArray *data = _receivedNotification.request.content.userInfo[@"qgPush"][@"custom"][@"data"];
        return data.count;
    } @catch (NSException *exception) {
        return 0;
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.03;
    } else if (option == iCarouselOptionWrap) {
        return 1;
    } else if (option == iCarouselOptionRadius) {
        return value * 1.1;
    } else if (option == iCarouselOptionArc) {
        if (carousel.type == iCarouselTypeCylinder) {
            return value * 0.8;
        }
        return value * 0.9;
    }
    return value;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel {
    if (_isSlider) {
        return slider_width + 1.5;
    } else {
        return 260 + 2;
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index withExtensionContext:(NSExtensionContext *)context API_AVAILABLE(ios(12.0)) {
    if (@available(iOS 12.0, *)) {
        NSDictionary *userInfo = self.receivedNotification.request.content.userInfo;
        NSDictionary *obj = userInfo[@"qgPush"][@"custom"][@"data"][index];
        
        // track qg_carousel_clicked
        [[QGNotificationEvents instance] trackCarouselItemClicked:userInfo withItem:obj];
        
        if (obj[@"deepLink"]) {
            NSString *urlString = obj[@"deepLink"];
            [[QGNotificationEvents instance] saveUserDefaultsWithValue:urlString Key:QG_EXT_RICH_PUSH_DEEPLINK];
        }
        [context performNotificationDefaultAction];
    }
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate API_AVAILABLE(ios(12.0)) {
    // track notification_browsed
    [[QGNotificationEvents instance] trackNotificationBrowsed:_receivedNotification.request.content.userInfo];
}

//supported carousel types
- (iCarouselType)getCarouselType:(NSString *)str {
    if ([str caseInsensitiveCompare:@"Linear"] == NSOrderedSame) {
        return iCarouselTypeLinear;
    } else if ([str caseInsensitiveCompare:@"Rotary"] == NSOrderedSame) {
        return iCarouselTypeRotary;
    } else if ([str caseInsensitiveCompare:@"Cylinder"] == NSOrderedSame) {
        return iCarouselTypeCylinder;
    } else if ([str caseInsensitiveCompare:@"CoverFlow"] == NSOrderedSame) {
        return iCarouselTypeCoverFlow2;
    } else
        return iCarouselTypeLinear;
}

//image content mode aspect fit/fill
- (UIViewContentMode)getContentMode:(NSString *)string {
    if ([string caseInsensitiveCompare:@"fit"] == NSOrderedSame) {
        return UIViewContentModeScaleAspectFit;
    } else if ([string caseInsensitiveCompare:@"fill"] == NSOrderedSame) {
        return UIViewContentModeScaleAspectFill;
    } else
        return UIViewContentModeScaleAspectFit;
}

- (NSString *)getDeepLinkForNotification:(UNNotification *)notification atIndex:(NSInteger)index {
    UNNotificationContent *content = notification.request.content;
    NSDictionary *obj = content.userInfo[@"qgPush"][@"custom"][@"data"][index];
    if (obj[@"deepLink"]) {
        return obj[@"deepLink"];
    }
    return nil;
}

- (void)extensionLogEvent:(NSString *)name withParameters:(NSDictionary *)parameters withValueToSum:(NSNumber *)valueToSum {
    [[QGNotificationEvents instance] extensionLogEvent:name withParameters:parameters withValueToSum:valueToSum];
}

#pragma mark - Helpers

//Image Resizing for Slider Push
//Resize the first image and fix the Slider height
- (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    float maxHeight = slider_width;
    float newHeight = sourceImage.size.height * scaleFactor;
    newHeight = newHeight >= maxHeight ? maxHeight : newHeight;
    
    UIGraphicsBeginImageContext(CGSizeMake(i_width, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, i_width, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)fixCarouselHeightForSlider {
    UNNotificationAttachment *attachment = [self.receivedNotification.request.content.attachments objectAtIndex:0];
    if (attachment.URL.startAccessingSecurityScopedResource) {
        UIImage *image = [UIImage imageWithContentsOfFile:attachment.URL.path];
        //70 pixel for title and body - vertical
        UIImage *scaledImage = [self imageWithImage:image scaledToWidth:slider_width];
        slider_height = scaledImage.size.height + 70;
        [self setHeightConstraint:slider_height + 5];
        [attachment.URL stopAccessingSecurityScopedResource];
    }
}

- (void)setHeightConstraint:(float)height {
    //change carousel height and set constraint
    NSLayoutConstraint *heightConstraint;
    for (NSLayoutConstraint *constraint in self.carousel.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            heightConstraint = constraint;
        }
    }
    heightConstraint.constant = height;
}

@end
